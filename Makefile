install: transparent-shell-theme
	mkdir -p $(DESTDIR)$(PREFIX)/share/themes
	cp -r transparent-shell-theme $(DESTDIR)$(PREFIX)/share/themes
	chmod 755 $(DESTDIR)$(PREFIX)/share/themes
	
uninstall:
	rm -rf $(DESTDIR)$(PREFIX)/share/themes/transparent-shell-theme
